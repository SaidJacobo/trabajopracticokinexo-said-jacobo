﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TrabajoPracticoKinexo.Models
{
    public class CategoriaModel
    {
        public int Id { get; set; }

        [Display(Name = "Nombre")]
        [Required]
        public string Nombre { get; set; }

        [Display(Name = "Descripcion")]
        [Required]
        public string Descripcion { get; set; }

    }
}