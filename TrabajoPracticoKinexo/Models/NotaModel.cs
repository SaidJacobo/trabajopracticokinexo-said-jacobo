﻿using Blog.Contract.Comentarios;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TrabajoPracticoKinexo.Models
{
    public class NotaModel
    {
        public int Id { get; set; }

        [Display(Name = "Titulo")]
        [Required]
        public string Titulo { get; set; }

        [Display(Name = "Subtitulo")]
        [Required]
        public string Subtitulo { get; set; }

        [Display(Name = "Cuerpo")]
        [Required]
        public string Cuerpo { get; set; }

        [Display(Name = "Categoria")]

        [Required]
        public int Categoria { get; set; }

        public string NombreCategoria { get; set; }

        public bool Activa { get; set; }

        public List<Comentario> Comentarios { get; set; }

        public int MeGusta { get; set; }

    }
}