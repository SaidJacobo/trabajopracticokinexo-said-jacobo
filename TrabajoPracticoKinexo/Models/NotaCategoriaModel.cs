﻿using Blog.Contract;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TrabajoPracticoKinexo.Models
{
    public class NotaCategoriaModel
    {
        public IEnumerable<Categoria> Categorias { get; set; }
        public PagedList<Nota> PaginaNotas { get; set; }
    }
}