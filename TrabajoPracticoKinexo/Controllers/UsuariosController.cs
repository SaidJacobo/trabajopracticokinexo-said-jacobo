﻿using Blog.Contract.Usuarios;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using TrabajoPracticoKinexo.Models;

namespace TrabajoPracticoKinexo.Controllers
{
    public class UsuariosController : Controller
    {

        private readonly IUsuarios UsuariosServicios;

        public UsuariosController(IUsuarios usuarios)
        {
            this.UsuariosServicios = usuarios;
        }
        // GET: Usuarios
        public ActionResult Index()
        {
            return View();
        }

        // GET: Usuarios/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Usuarios/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Usuarios/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Usuarios/Edit/5
        [Authorize(Roles = "Administrador")]

        public ActionResult Edit()
        {
            var Usuario = this.UsuariosServicios.RetornarUsuarioId(User.Identity.GetUserId());

            UsuarioModel usuario = new UsuarioModel() {Id = Usuario.Id, Mail = Usuario.Mail, Nombre = Usuario.Nombre, NumeroTelefono = Usuario.NumeroTelefono};
            return View(usuario);
        }

        // POST: Usuarios/Edit/5
        [Authorize(Roles = "Administrador")]
        [HttpPost]
        public ActionResult Edit(UsuarioModel usuario)
        {
            try
            {
                // TODO: Add update logic here

                Usuario Usuario = new Usuario {Id = usuario.Id, Mail = usuario.Mail, Nombre = usuario.Nombre, NumeroTelefono = usuario.NumeroTelefono };

                this.UsuariosServicios.EditarUsuario(Usuario);

                return RedirectToAction("Index", "Home");
            }
            catch
            {
                return View();
            }
        }


        // POST: Usuarios/Delete/5
        [HttpPost]
        public JsonResult Delete(UsuarioModel usuario)
        {
            Request.GetOwinContext().Authentication.SignOut();
            this.UsuariosServicios.EliminarUsuario(usuario.Id);
            return Json(new { ok = "La cuenta se elimino correctamente." }, JsonRequestBehavior.AllowGet);
        }
    }
}
