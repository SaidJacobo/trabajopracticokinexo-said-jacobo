﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TrabajoPracticoKinexo.Models;
using Blog.Contract;

namespace TrabajoPracticoKinexo.Controllers
{
    [Authorize(Roles = "Administrador")]
    public class CategoriasController : Controller
    {
        private readonly ICategorias CategoriasServicios;

        public CategoriasController(ICategorias categorias)
        {
            this.CategoriasServicios = categorias;
        }

        public ActionResult Index()
        {
            List<CategoriaModel> listaCategorias = CategoriasServicios.RetornarCategorias()
                                                    .Select(x => new CategoriaModel
                                                    { Id = x.Id, Nombre = x.Nombre, Descripcion = x.Descripcion }).ToList();
            return View(listaCategorias);
        }

        [Authorize(Roles = "Administrador")]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "Administrador")]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CategoriaModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            Categoria categ = new Categoria { Id = model.Id, Descripcion = model.Descripcion, Nombre = model.Nombre };
            this.CategoriasServicios.CrearCategoria(categ);
            return RedirectToAction("Index");
        }

        [Authorize(Roles = "Administrador")]
        public ActionResult Edit(int id)
        {
            Categoria categ = CategoriasServicios.RetornarCategorias().SingleOrDefault(x => x.Id == id);
            CategoriaModel categoria = new CategoriaModel { Id = categ.Id, Nombre = categ.Nombre, Descripcion = categ.Descripcion };

            return View(categoria);
        }

        [HttpPost]
        [Authorize(Roles = "Administrador")]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(CategoriaModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            Categoria categoria = new Categoria { Id = model.Id, Nombre = model.Nombre, Descripcion = model.Descripcion };
            this.CategoriasServicios.EditarCategoria(categoria);

            return RedirectToAction("Index");
        }

        [HttpPost]
        [Authorize(Roles = "Administrador")]
        public JsonResult EliminarCategoria(int id)
        {
            Categoria Categoria = this.CategoriasServicios.RetornarCategoriaId(id);
            this.CategoriasServicios.EliminarCategoria(Categoria);

            return Json(new { ok = "La categoria fue eliminada con exito" }, JsonRequestBehavior.AllowGet);
        }


    }
}