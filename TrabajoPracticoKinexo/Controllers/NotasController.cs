﻿using Blog.Contract;
using Blog.Contract.Notas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using TrabajoPracticoKinexo.Models;
using Blog.Contract.Comentarios;

namespace TrabajoPracticoKinexo.Controllers
{
    public class NotasController : Controller
    {
        private readonly INotas NotasServicios;
        private readonly ICategorias CategoriasServicios;
        private readonly IComentarios ComentariosServicios;

        public NotasController(INotas serviciosNotas, ICategorias ServiciosCategorias, IComentarios comentarios)
        {
            this.NotasServicios = serviciosNotas;
            this.CategoriasServicios = ServiciosCategorias;
            this.ComentariosServicios = comentarios;
        }

        public ActionResult Index(int page = 1, int pageSize = 4)
        {
            IList<Nota> listaNotas = NotasServicios.RetornarNotas();
            NotaCategoriaModel notasCategorias = new NotaCategoriaModel();
            notasCategorias.PaginaNotas = new PagedList<Nota>(listaNotas, page, pageSize);
            notasCategorias.Categorias = this.CategoriasServicios.RetornarCategorias();

            return View(notasCategorias);
        }

        [Authorize(Roles = "Administrador")]
        public ActionResult Create()
        {
            ViewBag.Categoria = new SelectList(CategoriasServicios.RetornarCategorias().ToList(),
                                                nameof(Categoria.Id), nameof(Categoria.Nombre));
            return View();
        }

        [HttpPost]
        [ValidateInput(false)]
        [Authorize(Roles = "Administrador")]
        [ValidateAntiForgeryToken]
        public ActionResult Create(NotaModel model)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.Categoria = new SelectList(CategoriasServicios.RetornarCategorias().ToList(),
                                    nameof(Categoria.Id), nameof(Categoria.Nombre));
                return View(model);
            }

            Nota nota = new Nota { Titulo = model.Titulo, Subtitulo = model.Subtitulo,
                                   Cuerpo = Server.HtmlEncode(model.Cuerpo), Categoria = model.Categoria, NombreCategoria = this.CategoriasServicios.RetornarCategoriaId(model.Categoria).Nombre};

            this.NotasServicios.CrearNota(nota);
            return RedirectToAction("Index");
        }

        [Authorize(Roles = "Administrador")]
        public ActionResult Edit(int id)
        {   
            var nota = NotasServicios.RetornarNotas().SingleOrDefault(x => x.Id == id);
            NotaModel notaModel = new NotaModel { Titulo = nota.Titulo, Subtitulo = nota.Subtitulo,
                                                Categoria = nota.Categoria, Cuerpo = nota.Cuerpo,
                                                NombreCategoria = nota.NombreCategoria};

            ViewBag.Categoria = new SelectList(CategoriasServicios.RetornarCategorias().ToList(),
                                                nameof(Categoria.Id), nameof(Categoria.Nombre));

            return View(notaModel);
        }

        [HttpPost]
        [ValidateInput(false)]
        [Authorize(Roles = "Administrador")]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Nota model)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.Categoria = new SelectList(CategoriasServicios.RetornarCategorias().ToList(),
                                    nameof(Categoria.Id), nameof(Categoria.Nombre), model.Categoria);                
                return View(model);
            }

            Nota nota = new Nota { Titulo = model.Titulo, Subtitulo = model.Subtitulo, Cuerpo = model.Cuerpo, Categoria = model.Categoria, Id = model.Id,
                                    Activa = true, NombreCategoria = this.CategoriasServicios.RetornarCategoriaId(model.Categoria).Nombre };
            this.NotasServicios.EditarNota(nota);

            return RedirectToAction("Index");
        }

        [Authorize(Roles = "Administrador")]
        public ActionResult VerNota(int id)
        {
            var nota = this.NotasServicios.RetornarNotaId(id);
            NotaModel Nota = new NotaModel
            {
                Titulo = nota.Titulo,
                Categoria = nota.Categoria,
                Cuerpo = nota.Cuerpo,
                Subtitulo = nota.Subtitulo,
                Id = id,
                NombreCategoria = nota.NombreCategoria
            };

            Nota.Comentarios = new List<Comentario>();
            Nota.Comentarios.AddRange(this.ComentariosServicios.RetornarComentarios(id));

            return View(Nota);
        }

        [HttpPost]
        [Authorize(Roles = "Administrador")]
        public JsonResult EliminarComentario(int id)
        {
            this.ComentariosServicios.EliminarComentario(id);

            return Json(new { ok = "El comentario fue eliminado" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [Authorize(Roles = "Administrador")]
        public JsonResult EliminarNota(int id)
        {
            this.NotasServicios.EliminarNota(id);

            return Json(new { ok = "La nota fue eliminada" }, JsonRequestBehavior.AllowGet);
        }


    }
}