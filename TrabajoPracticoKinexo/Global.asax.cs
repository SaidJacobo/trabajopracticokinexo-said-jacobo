﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using SimpleInjector;
using Blog.Contract;
using Blog.Services;
using SimpleInjector.Integration.Web;
using SimpleInjector.Integration.Web.Mvc;
using Blog.Contract.Notas;
using Blog.Contract.Comentarios;
using Blog.Contract.Usuarios;

namespace TrabajoPracticoKinexo
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            var container = new Container();
            container.Options.DefaultLifestyle = new WebRequestLifestyle();
            container.Register<ICategorias, ServiciosCategorias>();
            container.Register<INotas, ServiciosNotas>();
            container.Register<IComentarios, ServiciosComentarios>();
            container.Register<IUsuarios, ServiciosUsuarios>();


            container.RegisterMvcControllers(System.Reflection.Assembly.GetExecutingAssembly());
            container.RegisterMvcIntegratedFilterProvider();
            DependencyResolver.SetResolver(new SimpleInjectorDependencyResolver(container));
        }
    }
}
