﻿using Blog.Contract.Comentarios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Blog.FrontEnd.Models
{
    public class NotaModel
    {
        public int Id { get; set; }

        public string Titulo { get; set; }

        public string Subtitulo { get; set; }

        public string Cuerpo { get; set; }

        public int Categoria { get; set; }

        public string NombreCategoria { get; set; }

        public List<Comentario> Comentarios { get; set; }

        public int MeGusta { get; set; }

    }
}