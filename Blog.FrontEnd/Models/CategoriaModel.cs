﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Blog.FrontEnd.Models
{
    public class CategoriaModel
    {
        public int Id { get; set; }

        public string Nombre { get; set; }

        public string Descripcion { get; set; }
    }
}