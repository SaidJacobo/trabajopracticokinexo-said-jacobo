﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog.FrontEnd.Models
{
    public class UsuarioModel
    {
        public string Id { get; set; }
        [Display(Name = "Nombre")]
        public string Nombre { get; set; }

        [Display(Name = "Mail")]
        public string Mail { get; set; }

        [Display(Name = "Numero de telefono")]
        public string NumeroTelefono { get; set; }
    }
}