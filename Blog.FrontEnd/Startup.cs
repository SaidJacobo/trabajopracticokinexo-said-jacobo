﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Blog.FrontEnd.Startup))]
namespace Blog.FrontEnd
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}