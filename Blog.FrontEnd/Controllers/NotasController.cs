﻿using Blog.Contract;
using Blog.Contract.Notas;
using Blog.FrontEnd.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using Blog.Contract.Comentarios;
using Microsoft.AspNet.Identity;

namespace Blog.FrontEnd.Controllers
{
    public class NotasController : Controller
    {
        private readonly INotas NotasServicios;
        private readonly ICategorias CategoriasServicios;
        private readonly IComentarios ComentariosServicios;


        public NotasController(INotas serviciosNotas, ICategorias serviciosCategorias, IComentarios comentarios)
        {
            this.NotasServicios = serviciosNotas;
            this.CategoriasServicios = serviciosCategorias;
            this.ComentariosServicios = comentarios;
        }

        public ActionResult Index(int page = 1, int pageSize = 4)
        {
            IList<Nota> listaNotas = NotasServicios.RetornarNotas();
            NotaCategoriaModel notasCategorias = new NotaCategoriaModel();
            notasCategorias.PaginaNotas = new PagedList<Nota>(listaNotas, page, pageSize);
            notasCategorias.Categorias = this.CategoriasServicios.RetornarCategorias();

            return View(notasCategorias);
        }


        [Authorize(Roles = "Usuario")]
        public ActionResult VerNota(int id)
        {
            var nota = this.NotasServicios.RetornarNotaId(id);
            NotaModel Nota = new NotaModel { Titulo = nota.Titulo, Categoria = nota.Categoria, Cuerpo = nota.Cuerpo, Subtitulo = nota.Subtitulo, Id = id,
                                            NombreCategoria = nota.NombreCategoria };

            Nota.Comentarios = new List<Comentario>();
            Nota.Comentarios.AddRange(this.ComentariosServicios.RetornarComentarios(id));

            return View(Nota);
        }

        public JsonResult DevolverMeGusta(int IdNota)
        {
            var CantidadMegusta = this.NotasServicios.RetornarNotaId(IdNota).MeGusta;
            return Json(new {cantidad = CantidadMegusta}, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult MeGusta(int IdNota)
        {
            var MeGusta = new MeGusta();
            MeGusta.IdNota = IdNota;
            MeGusta.IdUsuario = User.Identity.GetUserId();

            if (!this.NotasServicios.YaDioMegusta(MeGusta))
            {
                this.NotasServicios.DarMegusta(MeGusta);
                this.NotasServicios.MeGusta(IdNota);

                return Json(new { Mensaje = "Te gusta esta nota" }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { Mensaje = "Ya diste me gusta esta nota" }, JsonRequestBehavior.AllowGet);
        }
    }
}