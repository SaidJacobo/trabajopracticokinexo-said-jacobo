﻿using Blog.Contract.Usuarios;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Blog.FrontEnd.Models;

namespace Blog.FrontEnd.Controllers
{
    public class UsuariosController : Controller
    {

        private readonly IUsuarios UsuariosServicios;

        public UsuariosController(IUsuarios usuarios)
        {
            this.UsuariosServicios = usuarios;
        }
        // GET: Usuarios
        public ActionResult Index()
        {
            return View();
        }

        [Authorize(Roles = "Usuario")]
        public ActionResult Edit()
        {
            var Usuario = this.UsuariosServicios.RetornarUsuarioId(User.Identity.GetUserId());

            UsuarioModel usuario = new UsuarioModel() {Id = Usuario.Id, Mail = Usuario.Mail, Nombre = Usuario.Nombre, NumeroTelefono = Usuario.NumeroTelefono};
            return View(usuario);
        }

        // POST: Usuarios/Edit/5
        [Authorize(Roles = "Usuario")]
        [HttpPost]
        public ActionResult Edit(UsuarioModel usuario)
        {
            try
            {
                // TODO: Add update logic here

                Usuario Usuario = new Usuario {Id = usuario.Id, Mail = usuario.Mail, Nombre = usuario.Nombre, NumeroTelefono = usuario.NumeroTelefono };
                this.UsuariosServicios.EditarUsuario(Usuario);

                return RedirectToAction("Index", "Home");
            }
            catch
            {
                return View();
            }
        }


        // POST: Usuarios/Delete/5
        [HttpPost]
        public JsonResult Delete(UsuarioModel usuario)
        {
            Usuario Usuario = new Usuario { Id = usuario.Id, Mail = usuario.Mail, Nombre = usuario.Nombre, NumeroTelefono = usuario.NumeroTelefono };
            Request.GetOwinContext().Authentication.SignOut();
            this.UsuariosServicios.EliminarUsuario(Usuario.Id);
            return Json(new { ok = "La cuenta se elimino correctamente." }, JsonRequestBehavior.AllowGet);
        }
    }
}
