﻿using Blog.Contract.Comentarios;
using Blog.FrontEnd.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Blog.FrontEnd.Controllers
{
    public class ComentarioController : Controller
    {
        private readonly IComentarios ComentariosServicios;

        public ComentarioController (IComentarios comentarios)
        {
            this.ComentariosServicios = comentarios;
        }

        // GET: Comentario
        public ActionResult Index()
        {
            return View();
        }
        
        [ValidateInput(false)]
        [Authorize(Roles = "Usuario")]
        //[ValidateAntiForgeryToken]
        public ActionResult Comentar(int idNota)
        {
            ComentarioModel model = new ComentarioModel();
            model.NotaId = idNota;
            return View(model);
        }
        
        [HttpPost]
        [ValidateInput(false)]
        [Authorize(Roles = "Usuario")]
        [ValidateAntiForgeryToken]
        public ActionResult Comentar(ComentarioModel model)
        {
            Comentario Comentario = new Comentario{ Fecha = DateTime.Now, NotaId = model.NotaId, Texto = model.Texto,
                                                    UsuaroID = User.Identity.GetUserId(), NombreUsuario = User.Identity.GetUserName()};

            this.ComentariosServicios.CrearComentario(Comentario);
            return RedirectToAction("Index", "Notas");
        }

    }
}