﻿using Blog.Contract.Notas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Blog.Contract;
using Blog.DataAccess;
using System.Data.Entity.Validation;

namespace Blog.Services
{
    public class ServiciosNotas : INotas
    {
        public static IList<Nota> Notas = new List<Nota>();
 
        public void CrearNota(Nota nota)
        {
            try
            {
                // Your code...
                // Could also be before try if you know the exception occurs in SaveChanges
                BlogEntities contexto = new BlogEntities();
                contexto.Notas.Add(new Notas
                {
                    Activa = true,
                    Titulo = nota.Titulo,
                    Subtitulo = nota.Subtitulo,
                    Cuerpo = nota.Cuerpo,
                    Categoria = nota.Categoria,
                    NombreCategoria = nota.NombreCategoria
                });
                contexto.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    System.Diagnostics.Debug.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        System.Diagnostics.Debug.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            }
        }

        public IList<Nota> RetornarNotas()
        {
            BlogEntities contexto = new BlogEntities();
            var notas = contexto.Notas.Where(x => x.Activa);
            List<Nota> notasRes = new List<Nota>();

            foreach (var nota in notas)
            {
                notasRes.Add(new Nota
                {
                    Id = nota.Id_Notas,
                    Titulo = nota.Titulo,
                    Subtitulo = nota.Subtitulo,
                    Cuerpo = nota.Cuerpo,
                    Categoria = nota.Categoria,
                    NombreCategoria = nota.NombreCategoria,
                    MeGusta = contexto.Usuarios_MeGusta.Where(x => x.IdNota == nota.Id_Notas).Count()
                });
            }
            return notasRes;
        }

        public IList<Nota> RetornarNotas(int idCategoria)
        {
            BlogEntities contexto = new BlogEntities();
            var notas = contexto.Notas.Where(x => x.Activa && x.Categoria == idCategoria);
            List<Nota> notasRes = new List<Nota>();

            foreach (var nota in notas)
            {
                notasRes.Add(new Nota
                {
                    Id = nota.Id_Notas,
                    Titulo = nota.Titulo,
                    Subtitulo = nota.Subtitulo,
                    Cuerpo = nota.Cuerpo,
                    Categoria = nota.Categoria,
                    NombreCategoria = nota.NombreCategoria
                });
            }
            return notasRes;
        }


        public void EditarNota(Nota nota)
        {
            BlogEntities contexto = new BlogEntities();
            var Notas = contexto.Notas.Where(x => x.Activa == true && x.Id_Notas == nota.Id).SingleOrDefault();
            Notas.Activa = nota.Activa;
            Notas.Titulo = nota.Titulo;
            Notas.Subtitulo = nota.Subtitulo;
            Notas.Cuerpo = nota.Cuerpo;
            Notas.Categoria = nota.Categoria;
            Notas.NombreCategoria = nota.NombreCategoria;

            contexto.SaveChanges();
        }

        public void EliminarNota(int idNota)
        {
            BlogEntities contexto = new BlogEntities();
            var Notas = contexto.Notas.Where(x => x.Activa == true && x.Id_Notas == idNota).SingleOrDefault();
            Notas.Activa = false;
            contexto.SaveChanges();
        }

        public void RecargarNota(int idNota)
        {
            BlogEntities contexto = new BlogEntities();
            var Notas = contexto.Notas.Where(x => x.Activa == true && x.Id_Notas == idNota).SingleOrDefault();
            Notas.Activa = true;
            contexto.SaveChanges();
        }

        public Nota RetornarNotaId(int id)
        {
            BlogEntities contexto = new BlogEntities();
            var nota = contexto.Notas.Where(x => x.Activa && x.Id_Notas == id).SingleOrDefault();
            var Nota = new Nota {Activa = nota.Activa, Titulo = nota.Titulo, Categoria = nota.Categoria, Cuerpo = nota.Cuerpo, Id = nota.Id_Notas,
                                Subtitulo = nota.Subtitulo, NombreCategoria = nota.NombreCategoria, MeGusta = nota.Me_Gusta };
            return Nota;
        }

        public void MeGusta(int idNota)
        {
            BlogEntities contexto = new BlogEntities();
            var Notas = contexto.Notas.Where(x => x.Activa == true && x.Id_Notas == idNota).SingleOrDefault();
            Notas.Me_Gusta++;
            contexto.SaveChanges();
        }

        public void DarMegusta(MeGusta meGusta)
        {
            BlogEntities contexto = new BlogEntities();
            Usuarios_MeGusta megusta = new Usuarios_MeGusta { IdNota = meGusta.IdNota, IdUsuario = meGusta.IdUsuario };
            contexto.Usuarios_MeGusta.Add(megusta);
            contexto.SaveChanges();
        }

        public bool YaDioMegusta(MeGusta meGusta)
        {
            BlogEntities contexto = new BlogEntities();
            if (contexto.Usuarios_MeGusta.Where(x => x.IdUsuario == meGusta.IdUsuario && x.IdNota == meGusta.IdNota).SingleOrDefault() == null)
            {
                return false;
            }
            return true;
        }
    }
}
