﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Blog.Contract.Comentarios;
using Blog.DataAccess;

namespace Blog.Services
{
    public class ServiciosComentarios : IComentarios
    {
        public static IList<Comentario> Notas = new List<Comentario>();

        public void CrearComentario(Comentario comentario)
        {
            BlogEntities contexto = new BlogEntities();
            contexto.Comentarios.Add(new Comentarios
            {
                Activo = true, Fecha = DateTime.Now, NotaId = comentario.NotaId, Texto = comentario.Texto, UsuarioId = comentario.UsuaroID , NombeUsuario = comentario.NombreUsuario         
            });
            contexto.SaveChanges();
        }

        public void EliminarComentario(int id)
        {
            BlogEntities contexto = new BlogEntities();
            var Comentarios = contexto.Comentarios.Where(x => x.Id == id).SingleOrDefault();
            Comentarios.Activo = false;
            contexto.SaveChanges();
        }

        public IList<Comentario> RetornarComentarios(int idNota)
        {
            BlogEntities contexto = new BlogEntities();
            var comentarios = contexto.Comentarios.Where(x => x.Activo && x.NotaId == idNota);
            IList<Comentario> comentarioRes = new List<Comentario>();

            foreach (var comentario in comentarios)
            {
                comentarioRes.Add(new Comentario
                {
                    Id = comentario.Id, Activo = comentario.Activo, Fecha = comentario.Fecha, NotaId = comentario.NotaId,
                        Texto = comentario.Texto, UsuaroID = comentario.UsuarioId, NombreUsuario = comentario.NombeUsuario
                });
            }
            return comentarioRes;
        }
    }
}
