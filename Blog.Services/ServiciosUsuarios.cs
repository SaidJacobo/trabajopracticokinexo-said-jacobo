﻿using Blog.Contract.Usuarios;
using Blog.DataAccess;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blog.Services
{
    public class ServiciosUsuarios : IUsuarios
    {
        public void EditarUsuario(Usuario usuario)
        {
            BlogEntities contexto = new BlogEntities();
            var Usuarios = contexto.AspNetUsers.Where(x => x.Id == usuario.Id).SingleOrDefault();
            Usuarios.PhoneNumber = usuario.NumeroTelefono;
            Usuarios.Email = usuario.Mail;
            Usuarios.UserName = usuario.Nombre;
            contexto.SaveChanges();
        }

        public void EliminarUsuario(string Id)
        {
            try
            {

                BlogEntities contexto = new BlogEntities();
                var Usuarios = contexto.AspNetUsers.Where(x => x.Id == Id).SingleOrDefault();

                var Comentarios = contexto.Comentarios.Where(x => x.UsuarioId == Id);
                if (Comentarios != null)
                {
                    foreach (var comentario in Comentarios)
                    {
                        contexto.Comentarios.Remove(comentario);
                    }
                }

                var UsuariosMeGusta = contexto.Usuarios_MeGusta.Where(x => x.IdUsuario == Id);
                if (UsuariosMeGusta != null)
                {
                    foreach (var Umg in UsuariosMeGusta)
                    {
                        contexto.Usuarios_MeGusta.Remove(Umg);
                    }
                }

                var AspUserClaims = contexto.AspNetUserClaims.Where(x => x.UserId == Id);
                if (AspUserClaims != null)
                {
                    foreach (var auc in UsuariosMeGusta)
                    {
                        contexto.Usuarios_MeGusta.Remove(auc);
                    }
                }

                contexto.AspNetUsers.Remove(Usuarios);
                contexto.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    System.Diagnostics.Debug.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        System.Diagnostics.Debug.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            }

        }

        public Usuario RetornarUsuarioId(string idUsuario)
        {
            BlogEntities contexto = new BlogEntities();
            var Usuarios = contexto.AspNetUsers.Where(x => x.Id == idUsuario).SingleOrDefault();
            var usuario = new Usuario
            {
                Id = idUsuario,
                Mail = Usuarios.Email,
                Nombre = Usuarios.UserName,
                NumeroTelefono = Usuarios.PhoneNumber
            };

            return usuario;
        }
    }
}
