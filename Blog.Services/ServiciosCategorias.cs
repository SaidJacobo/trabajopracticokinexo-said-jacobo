﻿using Blog.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Blog.DataAccess;

namespace Blog.Services
{
    public class ServiciosCategorias : ICategorias
    {
        private static IList<Categoria> Categorias = new List<Categoria>();
        
        public IEnumerable<Categoria> RetornarCategorias()
        {
            BlogEntities contexto = new BlogEntities();
            var Categorias = contexto.Categorias.Where(x => x.Activa == true);
            var CategoriaRes = new List<Categoria>();

            foreach (var categoria in Categorias)
            {
                CategoriaRes.Add(new Categoria
                {
                    Id = categoria.Id_Categorias, Activa = categoria.Activa, Descripcion = categoria.Descripcion,
                    Nombre = categoria.Nombre
                });
            }

            return CategoriaRes;
        }

        public void CrearCategoria(Categoria categoria)
        {
            BlogEntities contexto = new BlogEntities();

            Categorias categorias = new Categorias
            {                
                Activa = true,
                Descripcion = categoria.Descripcion,
                Nombre = categoria.Nombre
            };
            contexto.Categorias.Add(categorias);  
            contexto.SaveChanges();
        }

        public void EditarCategoria(Categoria categoria)
        {
            this.Modificar(categoria, false);               
        }

        public void EliminarCategoria(Categoria categoria)
        {
            this.Modificar(categoria, true);
        }

        private void Modificar(Categoria categoria, bool eliminar)
        {
            BlogEntities contexto = new BlogEntities();
            var Categorias = contexto.Categorias.Where(x => x.Id_Categorias == categoria.Id).SingleOrDefault();

            if (Categorias != null)
            {
                if (eliminar)
                {
                    Categorias.Activa = false;
                }
                else
                {
                    Categorias.Descripcion = categoria.Descripcion;
                    Categorias.Nombre = categoria.Nombre;
                }
                contexto.SaveChanges();                
            }
        }

        public Categoria RetornarCategoriaId(int id)
        {
            BlogEntities contexto = new BlogEntities();
            var Categoria = contexto.Categorias.Where(x => x.Activa == true && x.Id_Categorias == id).SingleOrDefault();

            Categoria categoria = new Categoria { Activa = Categoria.Activa, Descripcion = Categoria.Descripcion, Id = id, Nombre = Categoria.Nombre };

            return categoria;
        }
    }
}
