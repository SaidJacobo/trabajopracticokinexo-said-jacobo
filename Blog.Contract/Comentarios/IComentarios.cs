﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blog.Contract.Comentarios
{
    public interface IComentarios
    {
        void CrearComentario(Comentario comentario);
        IList<Comentario> RetornarComentarios(int idNota);
        void EliminarComentario(int id);
    }
}
