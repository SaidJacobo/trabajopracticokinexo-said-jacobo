﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blog.Contract.Comentarios
{
    public class Comentario
    {
        public int Id { get; set; }
        public string UsuaroID { get; set; }
        public int NotaId { get; set; }
        public string Texto { get; set; }
        public bool Activo { get; set; }
        public DateTime Fecha { get; set; }
        public string NombreUsuario { get; set; }
    }
}
