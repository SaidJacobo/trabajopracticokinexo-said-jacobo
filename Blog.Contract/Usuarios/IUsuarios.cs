﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blog.Contract.Usuarios
{
    public interface IUsuarios
    {
        Usuario RetornarUsuarioId(string idUsuario);
        void EditarUsuario(Usuario usuario);
        void EliminarUsuario(string idUsuario);
    }
}
