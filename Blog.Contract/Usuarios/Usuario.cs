﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blog.Contract.Usuarios
{
    public class Usuario
    {
        public string Id{ get; set; }
        public string Nombre { get; set; }
        public string Mail { get; set; }
        public string NumeroTelefono { get; set; }
    }
}
