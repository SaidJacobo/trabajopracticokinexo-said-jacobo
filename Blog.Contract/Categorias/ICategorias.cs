﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blog.Contract
{
    public interface ICategorias
    {
        void CrearCategoria(Categoria categoria);

        void EditarCategoria(Categoria categoria);

        void EliminarCategoria(Categoria categoria);

        IEnumerable<Categoria> RetornarCategorias();

        Categoria RetornarCategoriaId(int id);
    }
}
