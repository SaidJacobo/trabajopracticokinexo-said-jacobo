﻿using Blog.Contract.Notas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blog.Contract.Notas
{
    public interface INotas
    {
        void CrearNota(Nota nota);
        void EditarNota(Nota nota);
        IList<Nota> RetornarNotas();
        void EliminarNota(int idNota);
        void RecargarNota(int idNota);
        Nota RetornarNotaId(int id);
        void MeGusta(int idNota);
        bool YaDioMegusta(MeGusta meGusta);
        void DarMegusta(MeGusta meGusta);
    }
}
