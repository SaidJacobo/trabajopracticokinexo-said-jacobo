﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blog.Contract
{
    public class Nota
    {
        public int Id { get; set; }

        public string Titulo { get; set; }

        public string Subtitulo { get; set; }

        public string Cuerpo { get; set; }

        public int Categoria { get; set; }

        public string NombreCategoria { get; set; }

        public bool Activa { get; set; }

        public int MeGusta { get; set; }
    }
}
