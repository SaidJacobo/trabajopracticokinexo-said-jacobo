﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blog.Contract.Notas
{
    public class MeGusta
    {
        public string IdUsuario { get; set; }
        public int IdNota { get; set; }
    }
}
